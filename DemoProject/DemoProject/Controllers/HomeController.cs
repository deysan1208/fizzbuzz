﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DemoProject.Models;

namespace DemoProject.Controllers
{
    public class HomeController : Controller
    {
        bool flag = false;

        public ActionResult Index()
        {
            ViewBag.flagvalue = flag;
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection fc, UserModel um)
        {
            if (ModelState.IsValid)
            {
                um.Numbers = Convert.ToInt32(fc["Numbers"]);
                ViewBag.Num = um.Numbers;

                //int countAdd = 1;
                StringBuilder sb = new StringBuilder();

                for (int j = 1; j <= um.Numbers; j++)
                    {
                        string str = "";
                        if (j % 3 == 0)
                        {
                            str += "Fizz";
                        }
                        if (j % 5 == 0)
                        {
                            str += "Buzz";
                        }
                        if (str.Length == 0)
                        {
                            str = j.ToString();
                        }
                        // sb.Append(((j - 1) * 10 + countAdd) + " ");
                        sb.Append((str) + " ");
                    }
                    //sb.Append("<br />");
                    //countAdd++;
               

                ViewBag.ResultSet = sb.ToString();

                return View(um);
            }
            else
            {
                return View();
            }
        }
    }
}